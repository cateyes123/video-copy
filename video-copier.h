#pragma once

#include <string>
#include <thread>
#include <mutex>
#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>


class VideoCopier
{
public:
    static const size_t BUFFER_SIZE = 100;
    
public:
    VideoCopier(const std::string & infile, const std::string & outfile);
    ~VideoCopier();
    
public:
    void run();
    
private:
    void load();
    void save();
    
private:
    std::string infile_;
    std::string outfile_;
    size_t total_frame_count_;
    std::mutex mutex_;
    boost::circular_buffer<cv::Mat> buffer_;
    std::thread load_thread_;
    std::thread save_thread_;
    cv::VideoCapture video_capture_;
    cv::VideoWriter video_writer_;
};
