#include "video-copier.h"
#include <iostream>
#include <boost/format.hpp>


VideoCopier::VideoCopier(const std::string &infile, const std::string &outfile)
    : infile_(infile)
    , outfile_(outfile)
    , total_frame_count_(0)
    , buffer_(BUFFER_SIZE)
{
}

VideoCopier::~VideoCopier()
{
    if (load_thread_.joinable())
        load_thread_.join();
    if (save_thread_.joinable())
        save_thread_.join();
}

void VideoCopier::run()
{
    video_capture_.open(infile_);
    if (video_capture_.isOpened())
    {
        cv::Size size = cv::Size(
            static_cast<int>(video_capture_.get(CV_CAP_PROP_FRAME_WIDTH)),
            static_cast<int>(video_capture_.get(CV_CAP_PROP_FRAME_HEIGHT)));
        int fourcc = static_cast<int>(video_capture_.get(cv::CAP_PROP_FOURCC));
        int fps = static_cast<int>(video_capture_.get(cv::CAP_PROP_FPS));
        total_frame_count_ = static_cast<size_t>(video_capture_.get(cv::CAP_PROP_FRAME_COUNT));
        
        video_writer_ = cv::VideoWriter(outfile_, fourcc, fps, size);
        
        load_thread_ = std::thread(&VideoCopier::load, this);
        save_thread_ = std::thread(&VideoCopier::save, this);
    }
    else
    {
        std::cerr << "Video not found. " << infile_ << std::endl;
    }
}

void VideoCopier::load()
{
    boost::format loading_hint("Video [%s] is %3.2f%% loaded...");
    size_t frame_count = 1;
    
    while (frame_count < total_frame_count_)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        cv::Mat frame;
        while (!buffer_.full())
        {
            video_capture_ >> frame;
            if (frame.empty())
            {
                assert(frame_count == total_frame_count_);
                break;
            }
            
            buffer_.push_back(frame);
            frame_count++;
        }
        std::cout << (loading_hint % infile_ % (1.0f * frame_count / total_frame_count_ * 100)) << std::endl;
    }
}

void VideoCopier::save()
{
    size_t frame_count = 1;
    
    while (frame_count < total_frame_count_)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        while (!buffer_.empty())
        {
            video_writer_ << buffer_.front();
            buffer_.pop_front();
            frame_count++;
        }
    }
    assert(frame_count == total_frame_count_);
}
