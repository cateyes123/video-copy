#include <iostream>
#include <memory>
#include <vector>
#include "video-copier.h"


int main(int argc, char * argv[])
{
    if (argc == 1)
    {
        std::cerr << "Videos are required as parameters." << std::endl;
    }
    else
    {
        std::vector<std::shared_ptr<VideoCopier>> workers;
        for (size_t i = 1; i < argc; i++)
        {
            std::string infile(argv[i]);
            std::string outfile = std::string("out-") + infile;
            
            std::shared_ptr<VideoCopier> worker(new VideoCopier(infile, outfile));
            worker->run();
            
            workers.push_back(worker);
        }
    }
    return 0;
}
