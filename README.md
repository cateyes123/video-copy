Usage:
    ./multiIO video1 [video2 video3 ...]

	- The output file will be input name with a prefix 'out-'.
	  e.g. video1.avi -> out-video1.avi
