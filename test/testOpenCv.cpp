#include <gtest/gtest.h>
#include <opencv2/opencv.hpp>

TEST(OpenCvTest, ReadAvi)
{
	cv::VideoCapture video("VideoTest.avi");
	ASSERT_TRUE(video.isOpened());

	cv::Mat videoFrame;
	video >> videoFrame;
	EXPECT_FALSE(videoFrame.empty());
}

TEST(OpenCvTest, WriteAvi)
{
	cv::VideoCapture video("VideoTest.avi");
	ASSERT_TRUE(video.isOpened());
	cv::Size videoSize = cv::Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
	int fourcc = (int)video.get(cv::CAP_PROP_FOURCC);
	int fps = (int)video.get(cv::CAP_PROP_FPS);

	cv::VideoWriter videoOut("VideoTestOut.avi", fourcc, fps, videoSize);   // MPEG-4

	cv::Mat videoFrame;
    for (int i = 0; i < 90 && video.read(videoFrame); i++)
	{
		videoOut << videoFrame;
	}
}

#if 0
TEST(OpenCvTest, ShowAvi)
{
	cv::VideoCapture video("VideoTest.avi");
	ASSERT_TRUE(video.isOpened());

	cv::Size videoSize = cv::Size((int)video.get(CV_CAP_PROP_FRAME_WIDTH),(int)video.get(CV_CAP_PROP_FRAME_HEIGHT));
	cv::namedWindow("video demo", CV_WINDOW_AUTOSIZE);
	cv::Mat videoFrame;

	int frameCount = 0;
	while(true){
		video >> videoFrame;
		if(videoFrame.empty()){
			break;
		}
		cv::imshow("video demo", videoFrame);
		cv::waitKey(1);
	}
}
#endif
